#!/usr/bin/env bash
# nbdkit
# Copyright (C) 2020 Red Hat Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
#
# * Neither the name of Red Hat nor the names of its contributors may be
# used to endorse or promote products derived from this software without
# specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY RED HAT AND CONTRIBUTORS ''AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL RED HAT OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
# USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
# OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

# Test the initial state of the memory plugin.

source ./functions.sh
set -e

requires nbdsh -c 'exit (not hasattr (h, "get_init_flags"))'

sock=`mktemp -u`
files="memory-init.pid $sock"
rm -f $files
cleanup_fn rm -f $files

# Run nbdkit with the memory plugin.
start_nbdkit -P memory-init.pid -U $sock memory 1M

# The image should start as sparse/zero, before we write to it
nbdsh --connect "nbd+unix://?socket=$sock" \
      -c 'assert (h.get_init_flags () == nbd.INIT_SPARSE | nbd.INIT_ZERO)' \
      -c 'h.pwrite ("hello world".encode (), 0)'

# The image is still sparse, but no longer zero, before we fill it
nbdsh --connect "nbd+unix://?socket=$sock" \
      -c 'assert (h.get_init_flags () == nbd.INIT_SPARSE)' \
      -c 'h.pwrite (("1" * (1024*1024)).encode (), 0)'

# The image is neither sparse nor zero, before we wipe it
nbdsh --connect "nbd+unix://?socket=$sock" \
      -c 'assert (h.get_init_flags () == 0)' \
      -c 'h.zero (1024 * 1024, 0)'

# Once again, the image is sparse/zero
nbdsh --connect "nbd+unix://?socket=$sock" \
      -c 'assert (h.get_init_flags () == nbd.INIT_SPARSE | nbd.INIT_ZERO)'
