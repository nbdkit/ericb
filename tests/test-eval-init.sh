#!/usr/bin/env bash
# nbdkit
# Copyright (C) 2020 Red Hat Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
#
# * Neither the name of Red Hat nor the names of its contributors may be
# used to endorse or promote products derived from this software without
# specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY RED HAT AND CONTRIBUTORS ''AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL RED HAT OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
# USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
# OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

# Test the initial state of the eval plugin.

source ./functions.sh
set -e

requires nbdsh -c 'exit (not hasattr (h, "get_init_flags"))'

# All four combinations of initial flags. Relies on values of NBD protocol.
for val in 0 1 2 3; do
    nbdkit -v -U - eval \
       get_size='echo 1024' \
       pread='dd if=/dev/zero count=$3 iflag=count_bytes' \
       init_sparse="exit $((3 * !($val & 1)))" \
       init_zero="exit $((3 * !($val & 2)))" \
       --run 'nbdsh --uri $uri -c "assert (h.get_init_flags () == '$val')"'
done

# Omitting is the same as returning false (the safe default).
nbdkit -v -U - eval \
   get_size='echo 1024' \
   pread='dd if=/dev/zero count=$3 iflag=count_bytes' \
   --run 'nbdsh --uri $uri -c "assert (h.get_init_flags () == 0)"'
